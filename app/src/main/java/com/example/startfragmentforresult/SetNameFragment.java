package com.example.startfragmentforresult;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class SetNameFragment  extends Fragment {

    Button finish_button;
    EditText name_input;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_set_name,container,false);
        finish_button = view.findViewById(R.id.finish_button);
        name_input = view.findViewById(R.id.name_input);

        finish_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                returnWithName();
            }
        });


        return  view;
    }


    private void returnWithName() {
        String name = name_input.getText().toString();

        ((DisplayNameFragment) getTargetFragment()).setName(name);
        getActivity().getSupportFragmentManager().popBackStackImmediate();
    }



}
