package com.example.startfragmentforresult;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class DisplayNameFragment extends Fragment {

    Button set_name_button;
    private int SET_NAME_REQUEST_CODE = 1;
    TextView name_textview;
    String name = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        set_name_button = view.findViewById(R.id.set_name_button);
        name_textview = view.findViewById(R.id.name_textview);

        set_name_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchSetNameFragment();
            }
        });

        return view;
    }

    void replace(Fragment fragment, String tag) {
        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment, tag)
                .addToBackStack(tag)
                .commit();
    }

    void launchSetNameFragment() {
        SetNameFragment newFragment = new SetNameFragment();
        String tag = "name";

        newFragment.setTargetFragment(this, SET_NAME_REQUEST_CODE);
        replace(newFragment, tag);
    }

    @Override
    public void onResume() {
        super.onResume();
        name_textview.setText(getString(R.string.your_name_is).format(name));
    }

    void setName(String name) {
        this.name = name;
    }


}
